---
title:
  - HECTOR Subsidiary Data Science Project
author:
  - MADS - UOS2
institute:
  - Koen van Esterik
  - Youri Dibbet

aspectratio:
  - 169
fonttheme:
  - structurebold
---

# Business Understanding

Gain a better understanding of the HECTOR project of hydrogen fuel cell powered RCV operating in Duisburg Germany by leveraging their dataset in order to optimize energy consumption.

## Research Question

What are the necessary and sufficient circumstances for reducing energy consumption of the mentioned RCV?

- What are the predictors of energy consumption?
- Which model fits best in this situation?
- How can we optimize the energy consumption of the RCV?

## Methodology

Train a model to predict energy consumption based on circumstances. By creating a model we will be able to simulate different scenarios enabling us to explore and evaluate alternative strategies for RCV.

- Weather conditions
- RCV usage
- Location circumstances
- A combination of the above

The machine learning algorithm required is a **supervised learning** algorithm that can handle **multiple regression**.

# Data Understanding

The provided dataset is time series based with roughly a quarter of a second interval. It is relatively large and contains multiple nan values. We are wondering if we can aggregate the data to a daily level, as the suggested model won't benefit from the high frequency data.

## Preparation

Preprocess data:

- Clean data
- Import air temperature data
- Engineer new features:
  - Vehicle distance delta
  - Road slope
  - Weight processed waste (todo)
  - Total power use (target feature)
- Aggregate to daily data

## Modeling

Train these models with preprocessed data:

- Linear regression
- SVM (todo)
- Random forest (todo)
- XGBoost (todo)

# Results

## Visualizations

![Predictors of Energy Consumption](predictors-of-energy-consumption.png){ height=70% } 

## Training Results

![Linear Regression](linear-regression.png){ height=70% }

# Next Steps

- How do we actually use a trained model to simulate different scenarios?
- What is your opinion on our research question? Does it make sense?
- What is energy consumption in your eyes? 
- Should we focus on hydrogen fuel consumption instead of total power use?


