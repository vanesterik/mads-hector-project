# Reports

This folder contains all reports and documentation related to the project. The following reports are available:

- [README.md](README.md): This document
- [project-setup.md](project-setup.md): Overview of the project setup
- [presentation.md](presentation.md): Presentation slides of current project status
  
## Document Conversion

The documents are written in Markdown and converted to PDF using [Pandoc](https://pandoc.org/). Below an overview of various commands to convert the documents to PDF.

### Report Document

Use the following command to convert a report document to PDF with properly resolved citations and figures:

```bash
pandoc \
01_exploratory_data_analysis.md \
--bibliography=citations/citations.bib \
--citeproc \
--csl=citations/ieee.csl \
--include-in-header=templates/disable_float.tex \
--listings \
--output=01_exploratory_data_analysis.pdf \
--resource-path=figures
```

### Presentation Document

Use the following command to convert a presentation document to PDF with properly resolved figures:

```bash
pandoc \
presentation.md \
-t beamer \
--output=presentation.pdf \
--resource-path=figures \
--slide-level=2
```