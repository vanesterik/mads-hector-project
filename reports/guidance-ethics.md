# Guidance Ethics

## Phase 1: Technology in Context

### Technology

| Used                                     | Produced                                              |
| ---------------------------------------- | ----------------------------------------------------- |
| Hector dataset (original)                | Machine learning canvas                               |
| Multiple libs (ie. Polars, Pandas, etc.) | Knowledge base (ie. repository)                       |
|                                          | Preprocessed dataset (imported & engineered features) |
|                                          | Predictive model                                      |
|                                          | Research Report                                       |

### Context

| External Factors                    |
| ----------------------------------- |
| Municipality legislation            |
| Climate change politics             |
| Transparency (ie. open source code) |


## Phase 2: Actors, Stakeholders

| Actors    | Stakeholders    |
| --------- | --------------- |
| Developer | All Actors      |
| End User  | Employee        |
| Tester    | Human Resources |
|           | Legal           |
|           | Supplier        |
|           | Traffic         |





