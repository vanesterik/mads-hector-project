---
title:
   - HECTOR Subsidiary Data Science Project
institute: 
   - MADS - UOS2
author: 
   - Koen van Esterik
date: 
   - 2024-03-10
---

This document outlines the project setup for the approach of the subsidiary data science research conducted as part of the HECTOR project [@pymanWorkPackageReport] at MADS - HAN University of Applied Sciences. The structure of the document is based and cross-validated on:
 
- CRISP-DM methodology [@CrossindustryStandardProcess2023]
- appendix A checklist in 'Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow' [@geronAgeronHandsonml32024]
- MADS project setup assessment 

By using this structure we aim to provide a clear and concise overview and to ensure that all relevant aspects are covered.

# Business Understanding

Our stakeholders requested to assess whether cost reduction is possible by analyzing the HECTOR dataset [@HANAIMDatasets2023] of a hydrogen fuel cell powered RCV operating in Duisburg Germany.

They are keen to understand if insights can be leveraged from the data, in order to optimize their business operation. These insights can be derived from predicting energy consumption based on circumstances: 

- weather conditions
- RCV usage
- location circumstances

The insights gained will allow them to make data-driven decisions to optimize operational planning and/or maintenance schedules. They would be able to develop strategies regarding - ie. when to drive, when to charge, etc. This is prospected to increase energy efficiency and reduce costs.

Currently there are no solutions in place to monitor and/or reduce energy consumption. Therefore this project could potentially be of high value to our stakeholders.

## Research Questions

With the aforementioned business objective in mind the following research question and sub questions can be formulated:

1. What are the necessary and sufficient circumstances for reducing energy consumption of the hydrogen fuel cell powered RCV? 
   a. Determine if weather conditions contribute to energy consumption
   b. Determine if RCV usage contribute to energy consumption
   c. Determine if location circumstances contribute to energy consumption
   d. Determine if a combination of the above contributes to energy consumption
   e. Determine the necessary and sufficient circumstances based on 1.a to 1d

## Research Methodology

To answer the questions above we are suggesting a system based on the following criteria:

- *supervised learning task*, as the model can ben trained with labeled examples
- *multiple regression*, as the model will use multiple features to make a prediction - ie. weather conditions, RCV usage, and location circumstances
- *multivariate regression*, as the model will be asked to predict multiple values - ie. energy consumption, weather conditions, RCV usage, and location circumstances

The system can be setup as a *batch learning system*, due to the following:

- There is no requirement of a continuous flow of data
- The data is not rapidly changing
- The data is small enough to fit in memory

![System pipeline design captured in a diagram](machine-learning-pipeline.png)

# Data Understanding

Our stakeholders have provided a dataset that includes:

- multivariate time series data
- time series recorded at sub-second intervals
- period spanning two years
- dataset contains of forty-two features (see Table 1)

```
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 46664942 entries, 0 to 46664941
Data columns (total 42 columns):
 #   Column                                          Dtype              
---  ------                                          -----              
 0   Bat_MaximumVoltage_x                            datetime64[us, UTC]
 1   Bat_MaximumVoltage_y                            float64            
 2   Bat_MinimumVoltage_y                            float64            
 3   Bat_SOC_y                                       float64            
 4   Bat_TotalCurrent_y                              float64            
 5   Bat_TotalVoltage_y                              float64            
 6   Body_Active_y                                   float64            
 7   Body_EmergencyStopActivated_y                   float64            
 8   Chas_AccPedalPos1000_y                          float64            
 9   Chas_BrakePedalPos1000_y                        float64            
 10  Chas_Signal_AirCon_SW_y                         float64            
 11  Chas_TotalVehicleDistance_m_y                   float64            
 12  Chas_Weight_Axle_1_y                            float64            
 13  Chas_Weight_Axle_2_y                            float64            
 14  Chas_Weight_Axle_3_y                            float64            
 15  Chas_Weight_Total_y                             float64            
 16  Data_Energy_CurrentEnergyBody_y                 float64            
 17  FCell_2_RX_air_flow_y                           float64            
 18  FCell_2_RX_cool_Temp_y                          float64            
 19  FCell_2_RX_Stack_Current_y                      float64            
 20  FCell_2_RX_Stack_Voltage_y                      float64            
 21  FCell_3_RX_air_flow_y                           float64            
 22  FCell_3_RX_cool_Temp_y                          float64            
 23  FCell_3_RX_Stack_Current_y                      float64            
 24  FCell_3_RX_Stack_Voltage_y                      float64            
 25  FCell_RX_air_flow_y                             float64            
 26  FCell_RX_cool_Temp_y                            float64            
 27  FCell_RX_Stack_Current_y                        float64            
 28  FCell_RX_Stack_Voltage_y                        float64            
 29  gps_altitude_y                                  float64            
 30  gps_latitude_y                                  float64            
 31  gps_longitude_y                                 float64            
 32  H2_Fill_ALL_y                                   float64            
 33  H2_Press_ALL_y                                  float64            
 34  H2_Temp_ALL_y                                   float64            
 35  H2_Weight_ALL_y                                 float64            
 36  HVS3_RX_Result_I_Value_y                        float64            
 37  MINVa_RX_VP_Status1_1_ActualSpeed_y             float64            
 38  MINVa_RX_VP_Status1_1_ActualTorque_y            float64            
 39  MINVa_RX_VP_Status2_1_ActualcalcDC_INCurrent_y  float64            
 40  ToDisp_Drive_Temp_Motor_y                       float64            
 41  VBB_30_y                                        float64            
dtypes: datetime64[us, UTC](1), float64(41)
memory usage: 14.6 GB
```

\begin{center}
Table 1: Overview of all features
\end{center}
\vspace{0.5cm}

Some initial issues can be identified when preliminary reviewing the dataset:

- Significant amount of data spanning several weeks, and even a month, is missing or incomplete
- Not all features seem relevant to our research, but requires investigation
- Features related to weather conditions are missing (ie. air temperature)

All these issues need to be addressed during the *data preparation* phase of the project.

# Data Preparation

The following steps have been identified in order to prepare the data:

1. Filter out periods when the RCV was not in operation
2. Impute missing values using K-NN due to the large amount of missing values [@murtiKNearestNeighborKNN2019]
3. Engineer or import missing features - ie. air temperature
4. Normalize features by scaling

This data preparation process will be implemented into the system pipeline (see Figure 1) in order to ensure reproducibility.

# Modeling

The following steps have been identified in order to develop the final model:

1. Split dataset into training and test sets - which needs careful consideration since it is a time series dataset [@EvaluatingPointForecast]
2. Select and train multiple models on training set
3. Evaluate models by *cross-validation*
4. Fine-tune models by *randomized search* - this because the system is based on *multiple regression* with multiple combinations of hyperparameters of which we want to control the number of iterations
5. Analyze best models on errors
6. Evaluate final model on test set

Our main stakeholder has provided us with a research paper that describes multiple strategies to model time series based data [@limTemporalFusionTransformers2020]. This paper could help us in the selection of the model that will be used to predict the energy consumption of the RCV.

# Evaluation

The performance measure of the model will be the *root mean squared error* of the predictions, since the suggested is a *regression problem* modeling task.

$$
RMSE(X, h) = \sqrt{\frac{1}{m} \sum_{i=1}^{m} (h(x^{(i)}) - y^{(i)})^2}
$$

In case there are a lot of outliers in the data, we might want to consider using the *mean absolute error*. 

$$
MAE(X, h) = \frac{1}{m} \sum_{i=1}^{m} |h(x^{(i)}) - y^{(i)}|
$$

The performance measure will indicate in what degree we are able to answer the research question. This in turn will determine whether to business objective can be reached.

# Deployment

The deployment phase of the project is yet to be determined as this is part of the third semester of the MADS program. Stay tuned for updates.

# Abbreviations

- CRISP-DM: Cross-Industry Standard Process for Data Mining
- HECTOR: Hydrogen Waste Collection Vehicles in Northwest Europe
- K-NN: K-Nearest Neighbor
- MADS: Master of Applied Data Science
- RCV: Refuse Collection Vehicle

# References

Below a list references that are used for this project:
