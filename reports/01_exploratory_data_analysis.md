---
date: "2024-04-25"
title: "HECTOR Subsidiary Data Science Research: Exploratory Data Analysis"
author: "Koen van Esterik"
documentclass: "scrartcl"
---

# 0. Abstract

The urgency to mitigate climate change impacts requires contributions from all sectors worldwide, including the transport sector. The **H**ydrog**E**n Waste **C**ollec**T**ion Vehicles in N**O**rth West Eu**R**ope (HECTOR) project was initiated with this perspective in mind. This research presents an exploratory data analysis (EDA) study using a specific dataset from the HECTOR project to identify predictors of energy consumption.

The study focuses on a hydrogen fuel-powered refuse collection vehicle (RCV) operating in Duisburg, Germany, between August 1, 2022, and November 1, 2023. The dataset consists of multivariate time series data recorded at approximately 0.24-second intervals, comprising forty-two features related to GPS, vehicle parameters, fuel cell operation, and environmental conditions.

External factors such as road topology, traffic conditions, driving behavior, and ambient temperature are known to influence energy consumption. This study aims to investigate the relationship between key features (air temperature, road slope, throttle pedal position, and vehicle total load) and target features (motor power use and total power use) to gain insights into energy consumption patterns.

Results from the analysis reveal correlations between air temperature, throttle pedal position, vehicle total load, and motor power use or total power use. With the potential for leveraging insights derived from throttle pedal behavior to optimize waste collection operations and suggests future research directions to explore driving behavior clustering for energy-efficient waste collection strategies.

# 1. Introduction

According to the IPCC all sectors in the world need to contribute in the pursuit of reducing climate change [@calvinIPCC2023Climate2023]. The transport sector [@fuglestvedtClimateForcingTransport2008] is no exception. The **H**ydrog**E**n Waste **C**ollec**T**ion Vehicles in N**O**rth West Eu**R**ope (HECTOR) project [@pymanWorkPackageReport] was initiated with that perspective.

This research is an exploratory data analysis (EDA) study on the provided dataset [@HANAIMDatasets2023]. And is a subsidiary project in order to answer the question: **What are the predictors of energy consumption** in the context of HECTOR.

The results of our research will allow our business stakeholders to gain insights in their day to day operation. Insights that offer a better understanding of energy consumption and how to implement possible optimizations.

The conducted research is based on:

- a hydrogen fuel powered refuse collection vehicle (RCV)
- which operated in Duisburg Germany 
- between a time span of 01-08-2022 and 01-11-2023

The mentioned dataset is the main input for our EDA study. The dataset consists of the following criteria:

- multivariate time series data
- time series recorded at approximately 0.24 seconds intervals
- dataset contains forty-two features including the measurement groups: GPS, vehicle, fuel cell, stack, hydrogen, input, motor, power and compactor.

According to an earlier study [@decauwerEnergyConsumptionPrediction2015] energy consumption is influenced by external factors such as: road topology, traffic, driving style, ambient temperature, etc. 

By aligning this study with the previously stated research question, we aim to provide a potential answer by examining the relationship between the features _air temperature_, _road slop_, _throttle pedal_, and _vehicle total load_ with the target features of _motor power use_ and/or _total power use_. 

You might question this approach as the mentioned features in relation to energy consumption is in fact physics knowledge. But we want to establish if this knowledge holds when applied to the provided dataset. Therefore will this premise be the framework for this research.

# 2. Methodology

The applied methodology for analyzing the dataset follows the general workflow
summarized in Figure 1. We start by loading and preparing the dataset. Then various features are engineered in a specific order due to interdependency. We have identified six groups of engineered features:

1. Energy consumption (which will contain the target features)
2. Temporal
3. Operational
4. Environmental
5. Navigational
6. Spatial

![Workflow diagram](01_exploratory_data_analysis_methodology.png)

Each group contains one or multiple features and are required to complete the workflow. The workflow has been implemented in Python using Matplotlib [@MatplotlibVisualizationPython], Numpy [@NumPy], Polars [@Polars], Pandas [@PandasPythonData] and Seaborn [@SeabornStatisticalData]. We use Polars for loading the initial dataset due to the relative large size (over 42 million records), but continue with Pandas in order to be flexible when analyzing.

## 2.1. Energy Consumption

We need to engineer _motor power use_ and _total power use_ in order to research the relation between the mentioned features and energy consumption.
The provided dataset contains _compactor power use_ and _HVAC power use_, but is missing _motor power use_. By multiplying the present _total battery voltage_ with the _motor current_ we are able to calculate the _motor power use_. Adding all present and engineered _power use_ features results in the _total power use_.

## 2.2. Temporal

We use the supplied `strftime` function [@PolarsExprDt] of Polars to determine the _day of the week_ based on the _datetime_ feature. The _day of the week_ will allow us to isolate ranges of geographical data, which will determine potential routes.

## 2.3. Navigational

We use the _datetime_ feature filtered by geographical data to compute which periods of the total dataset has consecutive data. These consecutive time ranges filtered by _day of the week_ and cross-referenced with geographical data will allow to draw a route per day on a map. From these daily routes respective RCV routes can be identified and will be engineered as a categorical _route_ feature, which can be used as filter for all subsequent research.

## 2.4. Operational

The original dataset contains a _motor speed_ feature, but not with a _vehicle speed_ feature. According to the provided data description, we need to multiply the _motor speed_ with 0.175 in order to get the _vehicle speed_ in kilometers an hour. With this engineered feature we are able to derive _transmission gear_ as - positive is _drive_, zero is _neutral_ and negative is _reverse_. Both these operational engineered features contribute to calculate spacial typed features.

## 2.5. Spatial

The result of filtering the geographical data on the _drive transmission gear_ allows to calculate the _vehicle distance delta_. By passing the shift between two sets of _latitude_ and _longitude_ values of geographical data to the haversine formula [@HaversineFormula2024] will result in the _vehicle distance delta_ with the curvature of earth considered.
We are able to calculate the _road slope_ by passing the calculated _vehicle distance delta_ and the geographical _altitude_ data to the `arctan2` function [@NumpyArctan2NumPy]supplied by Numpy.

## 2.6. Environmental

In the provided dataset description [@HANAIMDatasets2023] it is mentioned that the _air temperature_ feature is unavailable. Which requires us to import that data from another source. The German Weather Service (DWD) [@gutzmannWetterdienstOpenWeather] has made local German air temperature data available for us to use. This external _air temperature_ data needs to be aggregated to a specified timescale in order to align with our original dataset.

With all these features engineered we are able to analyze the enriched dataset. This by cross-referencing the features with specified filters and timescale aggregation as suggested in table 1:

| Predictor          | Target          | Filters                  | Timescale  |
| ------------------ | --------------- | ------------------------ | ---------- |
| Air Temperature    | Total Power Use | Route: >none             | 1 minute   |
|                    |                 | Transmission Gear: drive |            |
|                    |                 |                          |            |
| Road Slope         | Motor Power Use | Altitude: >0.0           | 1 seconds  |
|                    |                 | Latitude: >0.0           |            |
|                    |                 | Longitude: >0.0          |            |
|                    |                 | Route: >none             |            |
|                    |                 | Transmission Gear: drive |            |
|                    |                 |                          |            |
| Throttle Pedal     | Motor Power Use | Route: >none             | 10 seconds |
|                    |                 | Transmission Gear: drive |            |
|                    |                 |                          |            |
| Vehicle Total Load | Motor Power Use | Route: >none             | 1 minute   |
|                    |                 | Transmission Gear: drive |            |

Table: Cross-reference features with specified filters and timescale

# 3. Results

The aim of this study was to investigate how the features of _air temperature_, _road slope_, _throttle pedal_, and _vehicle total load_ relate to the target features of _motor power use_ and/or _total power use_. We are able to understand where the RCV has been operating and how it performed, by using the categorical _route_ feature to draw a map as depicted in figure 2.

![Route Map](route-map.png)

Each color represents a different route and is used as categorization for all cross-references.

## 3.1. Air Temperature

When reviewing the cross-reference between _air temperature_ and _total power use_ applied to all different routes, a correlation can potentially be observed as depicted in figure 3.

![Relation between Air Temperature and Total Power Use](air-temperature.png){ width=55% }

The correlation is expressed as follows: _lower air temperature necessitates more energy for performance_. This statement holds even though the plot shows a lot of noise. The RCV is equipped with an **energy management system (EMS)** to regulate flow of energy between all consuming parts of the vehicle. This could be the explanation for the noise, but requires more in depth research.

## 3.2. Road Slope

When reviewing the cross-reference between _road slope_ and _motor power use_ applied to all different routes, the correlation is not so clear at all as depicted in figure 4.

![Relation between Road Slope and Motor Power Use](road-slope.png){ width=55% }

We expected an higher _motor power use_ when the road slope angles upwards, but this is not reflected in the plot. We therefore question if the sensor on the RCV is actually functioning properly, because it seems a dramatic angle upwards and downwards is captured by the sensor while moving forwards. Suggesting that the sensor is not mounted at the center of the vehicle but possibly at the far end or front - making it sensitive for rapid acceleration and deceleration.

## 3.3. Throttle Pedal

When reviewing the cross-reference between _throttle pedal_ and _motor power use_ applied to all different routes, a clear correlation can be seen as depicted in figure 5.

![Relation between Throttle Pedal and Motor Power Use](throttle-pedal.png){ width=55% }

You might state that it is quite evident that more energy is consumed when the _throttle pedal_ is pushed downwards, and you would be right. But it is still required to see this statement being confirmed in the context of this dataset. This is required to answer our research question.

## 3.4. Vehicle Total Load

When reviewing the cross-reference between _vehicle total load_ and _motor power use_ applied to all different routes, a correlation can potentially be observed as depicted in figure 6.

![Relation between Vehicle Load Total and Motor Power Use](vehicle-load-total.png){ width=55% }

Again as with the cross-reference between _air temperature_ and _total power use_ significant noise is present in the resulting analysis. We expect the EMS to be the diffusing factor in this cross-reference as well. Further research is required to isolate specific behaviors of the EMS in order to get a more clear result.

# 4. Discussion

As mentioned in the results section - the EMS affects the clarity of the results in terms of the cross-references. We would need more information on this system in order to mimic behavior and thereby isolated the potential correlations.

Furthermore the cross-reference between _road slope_ and _motor power use_ is disappointing. As mentioned - our suspicion is that the sensor logging the _altitude_ of the RCV is either not working properly or mounted incorrectly for its data to be used effectively. Possible mitigation is to import external elevation (ie. Google Maps) data based upon the available _latitude_, _longitude_ data and reiterate the cross-reference analysis.

# 5. Conclusion

All researched features except _road slope_ tend to show a correlation on energy consumption - either on _motor power use_ or _total power use_. However the EMS diffuses a clear relation for at least the cross-references containing _air temperature_ and _vehicle total load_. Further research into the EMS is required in order to draw a conclusion.

But the cross-reference between _throttle pedal_ and _motor power use_ do show the most promise in terms of reaching the business objective of applying insights derived from the dataset in order to optimize the business operation. 

Further research could be formulated as follows: Investigate if the _throttle pedal_ feature can be clustered into a spectrum from passive to aggressive driving behavior. This could potentially offer an indication how much waste is been collected per energy unit when applying a particular driving style.

# 6. Abbreviations

- EDA: Exploratory Data Analysis
- EMS: Energy Management System
- HECTOR: Hydrogen Waste Collection Vehicles in Northwest Europe
- HVAC: Heating, Ventilation, and Air Conditioning
- RCV: Refuse Collection Vehicle

# 7. References
