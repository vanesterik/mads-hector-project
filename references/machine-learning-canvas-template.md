# Machine Learning Canvas

Machine Learning Canvas fitted with CRISP-DM process.

## Business Understanding & Data Understanding

Understand the business problem and the data available.

| Business Objectives                                    | End-User Personas                                                      | Value Proposition                                                            |
| ------------------------------------------------------ | ---------------------------------------------------------------------- | ---------------------------------------------------------------------------- |
| What business objectives are we serving?               | Who are the end-users of the predictive model?                         | How do we help the end-users get the job done and satisfy their needs?       |
| What KPI will be improved and by how much?             | What are the jobs they need to get done?                               | How do we alleviate or eliminate their pains and create value through gains? |
| What is the business value and how can it be measured? | What are their immediate pains and potential gains in trying to do so? |                                                                              |

## Data Preparation, Modeling & Evaluation

Prepare, model and evaluate possible solutions.

| Model Evaluation                                                            | End-User Actions                                                                                     | Modeling Task                                           | Data Sources                                               |
| --------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- | ------------------------------------------------------- | ---------------------------------------------------------- |
| What methods and metrics can be used to evaluate the ‘offline’ performance? | How do predictions elicit actions (or decisions) that result in the proposed value for the end-user? | What are we going to predict and with what information? | Which raw data sources can we use (internal and external)? |
| What is the cost of being wrong?                                            |                                                                                                      | What is the type of machine learning problem?           | How and with what frequency should the data be collected?  |

## Deployment

Deploy, integrate and monitor solution.

| Monitoring and Value Testing                                                | Deployment and Integration                                                | Next Steps                                                            |
| --------------------------------------------------------------------------- | ------------------------------------------------------------------------- | --------------------------------------------------------------------- |
| What methods and metrics can be used to monitor ’online’ model performance? | How can the model be deployed and brought into production?                | What to do next in developing the canvas and progressing the project? |
| How is the actual business value measured?                                  | How is it exposed to the end-user and integrated in a product or service? |                                                                       |