---
Date: 2024-02-12 13:06:00
Iteration: 000
Designed for: MADS HECTOR Project
Designed by: Koen van Esterik
---

Machine Learning Canvas based on expressed needs of project stakeholders.

## Business Understanding & Data Understanding

Understand the business problem and the data available.

### Business Objectives

**What business objectives are we serving?**

Consuming energy as efficiently as possible. In this context energy efficiency is defined as the amount of garbage processed per unit of energy consumed.

**What KPI will be improved and by how much?**

Energy Efficiency Ratio (EER): This metric measures the amount of garbage processed per unit of energy consumed. It can be calculated as:

$$
EER = \frac{\text{Garbage Processed}}{\text{Energy Consumed}}
$$

A higher EER indicates better energy efficiency, as the garbage has processed  more garbage for the same amount of energy input.

**What is the business value and how can it be measured?**

Cost effectiveness should be considered as business value, which could be measured by monitoring purchases of hydrogen fuel. The variable price of hydrogen fuel should be considered in the calculation in order to get a precise measurement.

### End-User Personas

**Who are the end-users of the predictive model?**

Business managers and traffic 

**What are the jobs they need to get done?**

**What are their immediate pains and potential gains in trying to do so?**

### Value Proposition

**How do we help the end-users get the job done and satisfy their needs?**

**How do we alleviate or eliminate their pains and create value through gains?**

## Data Preparation, Modeling & Evaluation

Prepare, model and evaluate possible solutions.

### Model Evaluation

**What methods and metrics can be used to evaluate the ‘offline’ performance?**

**What is the cost of being wrong?**

### End-User Actions

**How do predictions elicit actions (or decisions) that result in the proposed value for the end-user?**

### Modeling Task

**What are we going to predict and with what information?**

**What is the type of machine learning problem?**

### Data Sources

**Which raw data sources can we use (internal and external)?**

**How and with what frequency should the data be collected?**

## Deployment

Deploy, integrate and monitor solution.

### Monitoring and Value Testing

**What methods and metrics can be used to monitor ’online’ model performance?**

**How is the actual business value measured?**

### Deployment and Integration

**How can the model be deployed and brought into production?**

**How is it exposed to the end-user and integrated in a product or service?**

### Next Steps

**What to do next in developing the canvas and progressing the project?**
