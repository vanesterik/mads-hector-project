# Third-party imports
from mads_datasets import AbstractDatasetFactory, DatasetFactoryProvider, DatasetType
from mads_datasets.settings import SecureDatasetSettings


def load_dataset() -> AbstractDatasetFactory:
    """
    Load garbage dataset from GitLab repository.

    Examples
    --------
    >>> garbage_data = load_dataset()
    >>> lf = pl.scan_parquet(garbage_data.filepath)
    >>> lf.collect()
    shape: (37_404_970, 42)
    ┌──────────────────────────┬────────────────────────────┐
    │ Bat_MaximumVoltage_x     ┆ Bat_MaximumVoltage_y │ ... │
    │ ------------------------ ┆ -------------------- │ --- │
    │ datetime[μs, UTC]        ┆ f64                  │ ... │
    ╞══════════════════════════╪══════════════════════╪═════╡
    │ 2022-08-01 04:04:12      ┆ 0.0                  │ ... │
    │ 2022-08-01 04:04:12.010  ┆ null                 │ ... │
    │ ...                      ┆ ...                  │ ... │
    └──────────────────────────┴──────────────────────┴─────┘

    """

    # the secure settings will prompt you for a gitlab PAT
    garbage_settings = SecureDatasetSettings(
        dataset_url="https://gitlab.com/api/v4/projects/49088521/repository/files/HECTORdata%2Eparquet/raw?lfs=true",
        filename="garbage.parq",
        name="garbage",
        keyaccount="gitlab-MADS-PAT",
        keyname="gitlab-MADS-PAT",
        digest="77e2b874b1cf2d29e13dc68a3e113c99",
        unzip=False,
    )

    garbage_data = DatasetFactoryProvider.create_factory(
        settings=garbage_settings, dataset_type=DatasetType.SECURE
    )
    garbage_data.download_data()

    return garbage_data
