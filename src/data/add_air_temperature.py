from datetime import datetime

# Third-party imports
import polars as pl
from wetterdienst import Parameter, Resolution
from wetterdienst.provider.dwd.observation import DwdObservationRequest

# Local imports
from .constants import column_names as cn


def add_air_temperature(
    lazy_frame: pl.LazyFrame,
    start_date: datetime = datetime(2022, 8, 1),
    end_date: datetime = datetime(2023, 5, 1),
) -> pl.LazyFrame:
    """
    Add air temperature data to dataset. The air temperature is retrieved from
    the DWD observation data.

    Note: Needs to be called before `rename_columns` due to the reference of the
    renamed column `datetime`.

    Examples
    --------
    >>> lf = add_air_temperature(lf)
    >>> lf.collect()
    shape: (37_404_970, 43)
    ┌──────────────────────────┬────────────────────────────┐
    │ datetime                 ┆ ... │ air_temperature      │
    │ ------------------------ ┆ --- │ -------------------- │
    │ datetime[μs, UTC]        ┆ ... │ f64                  │
    ╞══════════════════════════╪═════╪══════════════════════╡
    │ 2022-08-01 04:04:12      ┆ ... │ 18.0                 │
    │ 2022-08-01 04:04:12.010  ┆ ... │ 18.0                 │
    │ ...                      ┆ ... │ ...                  │
    └──────────────────────────┴─────┴──────────────────────┘

    """

    result = (
        DwdObservationRequest(
            parameter=Parameter.TEMPERATURE_AIR_MEAN_200,
            resolution=Resolution.HOURLY,
            start_date=start_date,
            end_date=end_date,
        )
        # Filter on station closest to garbage truck route
        .filter_by_station_id(station_id=("01303",))
    )

    # Preprocess temperature data and transform to lazy frame
    temperatures = next(result.values.query()).df.drop_nulls().lazy()

    return (
        lazy_frame.join(
            temperatures.select("date", "value"),
            left_on=pl.col(cn.DATETIME),
            right_on="date",
            how="left",
        ).with_columns(
            # Convert temperature from Kelvin to Celsius
            (pl.col("value") - 273.15)
            # Fill missing values with previous value
            .backward_fill()
            # Rename column
            .alias(cn.AIR_TEMPERATURE)
        )
        # Drop redundant column
        .drop("value")
    )
