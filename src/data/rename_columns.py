# Third-party imports
import polars as pl

# Local imports
from .constants import column_names


def rename_columns(lazy_frame: pl.LazyFrame) -> pl.LazyFrame:
    """
    Rename feature labels to more descriptive names.

    Examples
    --------
    >>> lf = rename_columns(lf)
    >>> lf.collect()
    shape: (37_404_970, 43)
    ┌──────────────────────────┬─────────────────────────────┐
    │ datetime                 ┆ compactor_active      │ ... │
    │ ------------------------ ┆ --------------------- │ --- │
    │ datetime[μs, UTC]        ┆ f64                   │ ... │
    ╞══════════════════════════╪═══════════════════════╪═════╡
    │ 2022-08-01 04:04:12      ┆ 0.0                   │ ... │
    │ 2022-08-01 04:04:12.010  ┆ null                  │ ... │
    │ ...                      ┆ ...                   │ ... │
    └──────────────────────────┴───────────────────────┴─────┘

    """
    return (
        lazy_frame
        # Date and time
        .rename({"Bat_MaximumVoltage_x": column_names.DATETIME})
        # Compactor
        .rename({"Body_Active_y": column_names.COMPACTOR_ACTIVE})
        .rename({"Data_Energy_CurrentEnergyBody_y": column_names.COMPACTOR_POWER_USE})
        # Fuel Cell
        .rename({"FCell_RX_air_flow_y": column_names.FUEL_CELL_1_AIR_FLOW})
        .rename({"FCell_RX_cool_Temp_y": column_names.FUEL_CELL_1_COOL_TEMPERATURE})
        .rename({"FCell_2_RX_air_flow_y": column_names.FUEL_CELL_2_AIR_FLOW})
        .rename({"FCell_2_RX_cool_Temp_y": column_names.FUEL_CELL_2_COOL_TEMPERATURE})
        .rename({"FCell_3_RX_air_flow_y": column_names.FUEL_CELL_3_AIR_FLOW})
        .rename({"FCell_3_RX_cool_Temp_y": column_names.FUEL_CELL_3_COOL_TEMPERATURE})
        # GPS
        .rename({"gps_altitude_y": column_names.GPS_ALTITUDE})
        .rename({"gps_latitude_y": column_names.GPS_LATITUDE})
        .rename({"gps_longitude_y": column_names.GPS_LONGITUDE})
        # Hydrogen
        .rename({"H2_Temp_ALL_y": column_names.TANK_TEMPERATURE})
        .rename({"H2_Press_ALL_y": column_names.TANK_PRESSURE})
        .rename({"H2_Fill_ALL_y": column_names.TANK_FILL_PERCENTAGE})
        .rename({"H2_Weight_ALL_y": column_names.FUEL_MASS})
        # Inputs
        .rename({"Body_EmergencyStopActivated_y": column_names.EMERGENCY_SWITCH})
        .rename({"Chas_AccPedalPos1000_y": column_names.THROTTLE_PEDAL})
        .rename({"Chas_BrakePedalPos1000_y": column_names.BRAKE_PEDAL})
        .rename({"Chas_Signal_AirCon_SW_y": column_names.HVAC_ACTIVE})
        .rename({"HVS3_RX_Result_I_Value_y": column_names.HVAC_CURRENT})
        # Motor
        .rename({"MINVa_RX_VP_Status1_1_ActualSpeed_y": column_names.MOTOR_SPEED})
        .rename({"MINVa_RX_VP_Status1_1_ActualTorque_y": column_names.MOTOR_TORQUE})
        .rename(
            {
                "MINVa_RX_VP_Status2_1_ActualcalcDC_INCurrent_y": column_names.MOTOR_CURRENT
            }
        )
        .rename({"ToDisp_Drive_Temp_Motor_y": column_names.MOTOR_TEMPERATURE})
        # Power
        .rename({"VBB_30_y": column_names.POWER_AUX_BATTERY_VOLTAGE})
        .rename({"Bat_MaximumVoltage_y": column_names.POWER_MAX_CHANGE_VOLTAGE})
        .rename({"Bat_MinimumVoltage_y": column_names.POWER_MIN_CHANGE_VOLTAGE})
        .rename({"Bat_SOC_y": column_names.DRIVE_BATTERY_STATE_OF_CHARGE})
        .rename(
            {"Bat_TotalCurrent_y": column_names.POWER_CURRENT_BATTERY_DIRECT_CURRENT}
        )
        .rename(
            {"Bat_TotalVoltage_y": column_names.POWER_VOLTAGE_BATTERY_DIRECT_CURRENT}
        )
        # Stack
        .rename({"FCell_RX_Stack_Current_y": column_names.STACK_1_CURRENT})
        .rename({"FCell_RX_Stack_Voltage_y": column_names.STACK_1_VOLTAGE})
        .rename({"FCell_2_RX_Stack_Current_y": column_names.STACK_2_CURRENT})
        .rename({"FCell_2_RX_Stack_Voltage_y": column_names.STACK_2_VOLTAGE})
        .rename({"FCell_3_RX_Stack_Current_y": column_names.STACK_3_CURRENT})
        .rename({"FCell_3_RX_Stack_Voltage_y": column_names.STACK_3_VOLTAGE})
        # Vehicle
        .rename({"Chas_Weight_Axle_1_y": column_names.VEHICLE_LOAD_AXLE_1})
        .rename({"Chas_Weight_Axle_2_y": column_names.VEHICLE_LOAD_AXLE_2})
        .rename({"Chas_Weight_Axle_3_y": column_names.VEHICLE_LOAD_AXLE_3})
        .rename({"Chas_Weight_Total_y": column_names.VEHICLE_LOAD_TOTAL})
        .rename({"Chas_TotalVehicleDistance_m_y": column_names.VEHICLE_DISTANCE})
    )
