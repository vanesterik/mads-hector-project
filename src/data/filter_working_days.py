# Third-party imports
import polars as pl

# Local imports
from .constants import column_names


def filter_working_days(lazy_frame: pl.LazyFrame) -> pl.LazyFrame:
    """
    Filter working days from dataset:
    - Include Monday to Friday
    - Exclude holidays
    - Exclude out-of-office hours

    Examples
    --------
    >>> lf = lf.filter_working_days()
    >>> lf.collect()
    shape: (33_863_757, 43)
    ┌──────────────────────────┬─────┐
    │ datetime                 ┆ ... │
    │ ------------------------ ┆ --- │
    │ datetime[μs, UTC]        ┆ ... │
    ╞══════════════════════════╪═════╡
    │ 2022-08-01 04:04:12      ┆ ... │
    │ 2022-08-01 04:04:12.010  ┆ ... │
    │ ...                      ┆ ... │
    └──────────────────────────┴─────┘

    """

    return (
        lazy_frame
        # Include Monday to Friday
        .filter(
            (pl.col(column_names.DATETIME).dt.weekday() >= 1)
            & (pl.col(column_names.DATETIME).dt.weekday() <= 5)
        )
        # Exclude holidays - used dates are all public holidays in Germany
        .filter(
            # Holidays in all years
            # New Year's Day - 1st of January
            (pl.col(column_names.DATETIME).dt.month() != 1)
            & (pl.col(column_names.DATETIME).dt.day() != 1)
            # Labour Day - 1st of May
            & (pl.col(column_names.DATETIME).dt.month() != 5)
            & (pl.col(column_names.DATETIME).dt.day() != 1)
            # German Unity Day - 3rd of October
            & (pl.col(column_names.DATETIME).dt.month() != 10)
            & (pl.col(column_names.DATETIME).dt.day() != 3)
            # Christmas Day - 25th of December
            & (pl.col(column_names.DATETIME).dt.month() != 12)
            & (pl.col(column_names.DATETIME).dt.day() != 25)
            # Boxing Day - 26th of December
            & (pl.col(column_names.DATETIME).dt.month() != 12)
            & (pl.col(column_names.DATETIME).dt.day() != 26)
            # Holidays specifically in 2023 due to varying dates
            # Good Friday - Friday before Easter Sunday
            & (pl.col(column_names.DATETIME).dt.year() != 2023)
            & (pl.col(column_names.DATETIME).dt.month() != 4)
            & (pl.col(column_names.DATETIME).dt.day() != 7)
            # Easter Monday - Monday after Easter Sunday
            & (pl.col(column_names.DATETIME).dt.year() != 2023)
            & (pl.col(column_names.DATETIME).dt.month() != 4)
            & (pl.col(column_names.DATETIME).dt.day() != 10)
            # Ascension Day - 40 days after Easter Sunday
            & (pl.col(column_names.DATETIME).dt.year() != 2023)
            & (pl.col(column_names.DATETIME).dt.month() != 5)
            & (pl.col(column_names.DATETIME).dt.day() != 18)
            # Whit Monday - 50 days after Easter Sunday
            & (pl.col(column_names.DATETIME).dt.year() != 2023)
            & (pl.col(column_names.DATETIME).dt.month() != 5)
            & (pl.col(column_names.DATETIME).dt.day() != 29)
        )
        # Exclude out-of-office hours - used times to filter are 04:00 to 12:00
        # because the logged data is in UTC time and needs to be converted to
        # CET time
        .filter(
            (pl.col(column_names.DATETIME).dt.hour() >= 4)
            & (pl.col(column_names.DATETIME).dt.hour() <= 12)
        )
    )
