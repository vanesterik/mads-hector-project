import pandas as pd
from datetime import datetime, timedelta, UTC
from pathlib import Path


def write_to_csv(df: pd.DataFrame, filename: str = None) -> None:
    """
    Write a pandas DataFrame to a csv file with timestamp a file name.

    Examples
    --------
    >>> write_to_csv(df)
    """

    # Define base path
    dirname = Path()
    # Define timestamp in correct timezone
    timestamp = datetime.now(UTC) + timedelta(hours=2)
    # Define filename if not provided by using timestamp
    filename = filename if filename else timestamp.strftime("%Y%m%d-%H%M") + ".csv"
    # Write to csv
    df.to_csv(dirname / filename, index=False)
