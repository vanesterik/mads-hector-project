from datetime import datetime, timedelta

# Third-party imports
import cartopy.crs as ccrs
from cartopy.io.img_tiles import GoogleTiles
import matplotlib.pyplot as plt
import pandas as pd

# Local imports
from .constants import column_names as cn, day_route_map as drm


def render_routes_map(
    df: pd.DataFrame, start_dates: list[str], end_dates: list[str]
) -> plt:
    """
    Render a map with routes for a given date range.

    Parameters
    ----------
    df : pd.DataFrame
        A pandas DataFrame with columns for GPS latitude, GPS longitude, datetime, and day of the week.
    start_dates : list[str]
        List of start dates for date range in the format "YYYY-MM-DD".
    end_dates : list[str]
        List of end dates for date range in the format "YYYY-MM-DD".

    Returns
    -------
    plt
        A matplotlib plot with the routes for the given date range.

    Examples
    --------
    >>> render_routes_map(df, start_dates, end_dates)
    """

    # Create a GoogleTiles object
    google_tiles = GoogleTiles("RGB", "street")

    # Set figure size
    fig = plt.figure(figsize=(16, 11))

    # Create a GeoAxes in the tile's projection
    ax = fig.add_subplot(1, 1, 1, projection=google_tiles.crs)

    # Define coordinates edges
    min_lat = 51.431628438858745
    min_lon = 6.673674931168041
    max_lat = 51.39142401852408
    max_lon = 6.771092760160929

    # Set the extent of the map based on min and max coordinates
    ax.set_extent(
        [
            min_lon,
            max_lon,
            min_lat,
            max_lat,
        ]
    )

    # Add the tiles at zoom level 11
    ax.add_image(google_tiles, 14)

    ##
    # Data Plotting
    ##

    # Create a list to hold the conditions for each date range
    conditions = []

    # Iterate over the lists of start dates and end dates
    for start_date, end_date in zip(start_dates, end_dates):
        start_timestamp = pd.Timestamp(start_date, tz="UTC")
        end_timestamp = pd.Timestamp(end_date, tz="UTC")

        # Create the condition for the current date range
        condition = (df[cn.DATETIME] >= start_timestamp) & (
            df[cn.DATETIME] <= end_timestamp
        )

        # Append the condition to the list of conditions
        conditions.append(condition)

    # Combine all conditions using logical OR (|)
    conditions = pd.concat(conditions, axis=1).any(axis=1)

    # Apply the combined condition to filter the DataFrame
    df = df[conditions]

    # Loop though day_color_map and add routes to map
    for day, props in drm.DAY_ROUTE_MAP.items():

        # Filter data by day
        df_day = df[df[cn.DAY_OF_THE_WEEK] == day]

        # Plot route work area if coordinates are provided
        if props["coordinates"]:
            ax.add_patch(
                plt.Rectangle(
                    (props["coordinates"][1], props["coordinates"][0]),
                    props["coordinates"][3] - props["coordinates"][1],
                    props["coordinates"][2] - props["coordinates"][0],
                    alpha=0.1,
                    facecolor=props["color"],
                    transform=ccrs.PlateCarree(),
                )
            )

        # Plot day route
        ax.scatter(
            data=df_day,
            x=cn.GPS_LONGITUDE,
            y=cn.GPS_LATITUDE,
            color=props["color"],
            label=props["color"],
            s=1,
            transform=ccrs.PlateCarree(),
        )

    ax.set_title(f"Routes from {start_dates[0]} to {end_dates[-1]}")
    ax.legend(loc="upper left", markerscale=4, title="Route")

    return plt


def get_weekly_date_range(
    start_date_str: str, end_date_str: str
) -> tuple[list[str], list[str]]:
    # Convert start_date_str and end_date_str to datetime objects
    start_date = datetime.strptime(start_date_str, "%Y-%m-%d")
    end_date = datetime.strptime(end_date_str, "%Y-%m-%d")

    # Initialize lists to store start dates and stop dates of each week
    weekly_start_dates = []
    weekly_stop_dates = []

    # Calculate start date of the first week (first Monday on or after start_date)
    current_date = start_date + timedelta(days=(7 - start_date.weekday() % 7))

    # Iterate through each week until reaching or exceeding end_date
    while current_date <= end_date:
        # Determine start date and stop date of the current week (Monday to Sunday)
        week_start = current_date
        week_stop = current_date + timedelta(days=6)

        # Append start date and stop date to the respective lists
        weekly_start_dates.append(week_start.strftime("%Y-%m-%d"))
        weekly_stop_dates.append(week_stop.strftime("%Y-%m-%d"))

        # Move to the start of the next week
        current_date += timedelta(days=7)

    return weekly_start_dates, weekly_stop_dates
