# Define color map for days of the week
DAY_ROUTE_MAP = {
    "Mon": {
        "color": "blue",
        "coordinates": [
            51.412100,  # min_lat
            6.714710,  # min_lon
            51.402000,  # max_lat
            6.727290,  # max_lon
        ],
    },
    "Tue": {
        "color": "orange",
        "coordinates": [
            51.42600,  # min_lat
            6.70000,  # min_lon
            51.41100,  # max_lat
            6.72730,  # max_lon
        ],
    },
    "Wed": {
        "color": "green",
        "coordinates": [
            51.41560,  # min_lat
            6.70800,  # min_lon
            51.40550,  # max_lat
            6.72000,  # max_lon
        ],
    },
    "Thu": {
        "color": "purple",
        "coordinates": [
            51.41300,  # min_lat
            6.72000,  # min_lon
            51.40200,  # max_lat
            6.73600,  # max_lon
        ],
    },
    "Fri": {
        "color": "red",
        "coordinates": [
            51.40850,  # min_lat
            6.70350,  # min_lon
            51.40000,  # max_lat
            6.72650,  # max_lon
        ],
    },
    "Sat": {
        "color": "yellow",
        "coordinates": [],
    },
    "Sun": {
        "color": "pink",
        "coordinates": [],
    },
}

# Extract route colors from map
ROUTE_COLORS = [props["color"] for day, props in DAY_ROUTE_MAP.items()]

# Define route colors dictionary from list
ROUTE_COLORS_DICT = dict(zip(ROUTE_COLORS, ROUTE_COLORS))
